/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                               *
 *  PHLOOP VER 2.1                                                                               *
 *  Simple image gallery                                                                         *
 *                                                                                               *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                               *
 *  Autor: Dmitry Poyarkov / strash.ru / 2015                                                    *
 *                                                                                               *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
 

function makeArrForGallery(contentNode, index, i) {
  'use strict';
  var j, counter, arrImg, arrTitle, arrImgLevel, arrTitleLevel;
  counter = 1;
  arrImg = [];
  arrTitle = [];
  arrImgLevel = [];
  arrTitleLevel = [];

  /*проставляем дивам атрибуты и айдишки*/
  contentNode.children[index].setAttribute('data-src-count', contentNode.children[index].children.length);
  contentNode.children[index].id = 'gallery' + i;

  /*записываем все изображения и подписи в массивы*/
  for (j = 0; j < contentNode.children[index].children.length; j++) {
    if (contentNode.children[index].children[j].title) {
      arrTitle[j] = contentNode.children[index].children[j].title;
    }
    arrImg[j] = contentNode.children[index].children[j].src;
  }

  /*чистка от пустых секций*/
  for (j = 0; j < arrImg.length; j++) {
    if (arrImg[j] === undefined) {
      arrImg.splice(j, 1);
      arrTitle.splice(j, 1);
      j--;
    }
  }

  /*пишем матрицы*/
  arrImgLevel[i] = arrImg.slice(0);
  arrTitleLevel[i] = arrTitle.slice(0);

  /*формируем галерею из счетчика, листалки, описания и первой картинки*/
  contentNode.children[index].innerHTML = '<span class="back">&#9667;</span><span class="pagging"></span><span class="clarification"></span><img>';
  contentNode.children[index].children[1].innerHTML = counter + '/' + contentNode.children[index].getAttribute('data-src-count');
  contentNode.children[index].children[3].src = arrImgLevel[i][0];
  if (arrTitleLevel[i][0] === undefined) {
    contentNode.children[index].children[2].style.opacity = '0';
  } else {
    contentNode.children[index].children[2].style.opacity = '';
    contentNode.children[index].children[2].innerHTML = arrTitleLevel[i][0];
    contentNode.children[index].children[2].title = arrTitleLevel[i][0];
  }

  /*имитируем background-size:cover и backround-position:center*/
  setJujube(contentNode.children[index], contentNode.children[index].children[3]);

  /*события мыши*/
  /*previous button*/
  contentNode.children[index].children[0].onmouseover = function (e) {
    e = e || event;
    this.style.backgroundColor = '#12b27b';
    this.nextSibling.style.backgroundColor = '';
    e.stopPropagation();
  };
  contentNode.children[index].children[0].onmouseout = function () {
    this.style.backgroundColor = '';
  };
  contentNode.children[index].children[0].onclick = function (e) {
    e = e || event;
    counter--;
    this.nextSibling.innerHTML = counter + '/' + this.parentNode.getAttribute('data-src-count');
    if (arrTitleLevel[this.parentNode.id.slice(7)][counter - 1] === undefined) {
      this.nextSibling.nextSibling.style.opacity = '0';
    } else {
      this.nextSibling.nextSibling.innerHTML = arrTitleLevel[this.parentNode.id.slice(7)][counter - 1];
      this.nextSibling.nextSibling.style.opacity = '';
    }
    this.nextSibling.nextSibling.nextSibling.src = arrImgLevel[this.parentNode.id.slice(7)][counter - 1];
    setJujube(this.parentNode, this.nextSibling.nextSibling.nextSibling);
    if (counter === 0) {
      counter = this.parentNode.getAttribute('data-src-count');
      this.nextSibling.innerHTML = counter + '/' + this.parentNode.getAttribute('data-src-count');
      if (arrTitleLevel[this.parentNode.id.slice(7)][counter - 1] === undefined) {
        this.nextSibling.nextSibling.style.opacity = '0';
      } else {
        this.nextSibling.nextSibling.innerHTML = arrTitleLevel[this.parentNode.id.slice(7)][counter - 1];
        this.nextSibling.nextSibling.title = arrTitleLevel[this.parentNode.id.slice(7)][counter - 1];
        this.nextSibling.nextSibling.style.opacity = '';
      }
      this.nextSibling.nextSibling.nextSibling.src = arrImgLevel[this.parentNode.id.slice(7)][counter - 1];
      setJujube(this.parentNode, this.nextSibling.nextSibling.nextSibling);
    }
    e.stopPropagation();
  };

  /*next button*/
  contentNode.children[index].onmouseover = function () {
    this.children[1].style.backgroundColor = '#12b27b';
  };
  contentNode.children[index].onmouseout = function () {
    this.children[1].style.backgroundColor = '';
  };
  contentNode.children[index].onclick = function () {
    if (counter >= this.getAttribute('data-src-count')) {
      counter = 0;
    }
    counter++;
    this.children[1].innerHTML = counter + '/' + this.getAttribute('data-src-count');
    if (arrTitleLevel[this.id.slice(7)][counter - 1] === undefined) {
      this.children[2].style.opacity = '0';
    } else {
      this.children[2].innerHTML = arrTitleLevel[this.id.slice(7)][counter - 1];
      this.children[2].title = arrTitleLevel[this.id.slice(7)][counter - 1];
      this.children[2].style.opacity = '';
    }
    this.children[3].src = arrImgLevel[this.id.slice(7)][counter - 1];
    setJujube(this, this.children[3]);
  };
}

function phloop(contentNode) {
  'use strict';
  var i, str, arrIndex;
  str = '';
  arrIndex = [];

  /*записываем классы всех потомков в строку*/
  for (i = 0; i < contentNode.children.length; i++) {
    str += contentNode.children[i].className;
  }

  /*если среди них есть галерея, то продолжаем*/
  if (str.indexOf('gallery') === -1) {
    return;
  }

  /*стираем данные из переменной*/
  str = '';

  /*записываем индексы в строку через запятую*/
  for (i = 0; i < contentNode.children.length; i++) {
    if (contentNode.children[i].className === 'gallery') {
      str += i + ',';
    }
  }

  /*переписываем индексы в одномерный массив и разделяем запятыми*/
  arrIndex = str.slice(0, -1).split(',');

  /*циклом проходимся по нужным дивам с картинками и запрашиваем функцию, которая формирует галерею*/
  for (i = 0; i < arrIndex.length; i++) {
    makeArrForGallery(contentNode, arrIndex[i], i);
  }
}